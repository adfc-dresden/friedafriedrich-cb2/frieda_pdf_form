<?php
/*
 * Plugin Name: Frieda & Friedrich PDF form for pickup
 * Author: Nils Larsen
 */

// Requires fpdf (tested with 1.8.6): fpdf.php in PLUGINDIR/lib/fpdf
// Requires fpdi (tested with 2.6.0): autoload.php in PLUGINDIR/lib/fpdi

use setasign\Fpdi\Fpdi;

require_once("FriedaBookingConfirmation.php");

function frieda_pdf_template_redirect() {

    $booking_post_name = filter_input(INPUT_GET, 'ausleihformular', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[a-z0-9]{24}$/')));

    if (empty($booking_post_name)) return;

    // Redirect via login page if no one is logged in
    if (get_current_user_id() <= 0) {
        $login_url = wp_login_url(home_url("?ausleihformular=$booking_post_name"));
        wp_redirect($login_url);
        exit;
    }

    $cb_booking_posts = get_posts(array(
        'post_type' => 'cb_booking',
        'post_status' => 'confirmed',
        'name' => $booking_post_name,
        'posts_per_page' => -1,
    ));

    if (empty($cb_booking_posts)) return;

    // Return if more than one booking matches the booking post name
    if (count($cb_booking_posts) != 1) return;

    $cb_booking = $cb_booking_posts[0];

    $user_id = $cb_booking->post_author; // id of user of booking

    // Return if given booking is not owned by current user
    if ($user_id != get_current_user_id()) {
        wp_die("Buchung gehört einem anderen Nutzer");
    }

    $item_id = get_post_meta($cb_booking->ID, "item-id", true);
    $item_name = html_entity_decode(get_the_title($item_id));

    $location_id = get_post_meta($cb_booking->ID, "location-id", true);
    $location_name = html_entity_decode(get_the_title($location_id));

    $address = get_user_meta($user_id, "address", true);

    $re = '/([^0-9]*[0-9]+[,]?)[ ]?(.*)/';
    $address2 = "";
    if (preg_match($re, $address, $matches, PREG_OFFSET_CAPTURE, 0) == 1) {
        $address = $matches[1][0];
        $address2 = $matches[2][0];
    }

    $phone = get_user_meta($user_id, "phone", true);
    $first_name = get_user_meta($user_id, "first_name", true);
    $last_name = get_user_meta($user_id, "last_name", true);
    $fullname = "$last_name, $first_name";

    $unix_time_start = get_post_meta($cb_booking->ID, 'repetition-start', true);
    $unix_time_end = get_post_meta($cb_booking->ID, 'repetition-end', true);

    $dateTimeStart = FriedaBookingConfirmation_dateTimeFromUnixtime($unix_time_start);
    $dateTimeEnd = FriedaBookingConfirmation_dateTimeFromUnixtime($unix_time_end);

    $bookingcode = get_post_meta($cb_booking->ID, '_cb_bookingcode', true);

    require_once(plugin_dir_path(__FILE__) . 'lib/fpdi/autoload.php');
    require_once(plugin_dir_path(__FILE__) . 'lib/fpdf/fpdf.php');

    $pdf = new Fpdi();

    $pdf->setSourceFile( $_SERVER['DOCUMENT_ROOT'] ."/files/Ausleihformular-Frieda-und-Friedrich.pdf");
    $tplId = $pdf->importPage(1);
    $size = $pdf->getTemplateSize($tplId);
    $pdf->AddPage($size['orientation'], $size);

    // use the imported page and place it at point 0,0
    $pdf->useTemplate($tplId, 0, 0);

    $x = 62;
    $y = 24.5;
    $dy = 5.35;
    $dy2 = 7.3;

    determine_and_set_font([$location_name, $item_name], $pdf, 45);

    // verleihstation
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($location_name)); 

    // Name Lastenrad
    $y += $dy;
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($item_name));

    determine_and_set_font([$fullname, $address, $address2, $phone], $pdf, 67);

    // Name, Vorname
    $y += $dy2;
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($fullname));

    // Straße, Hausnummer
    $y += $dy;
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($address));

    // PLZ Ort
    $y += $dy;
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($address2));

    // Telefonnummer
    $y += $dy;
    $pdf->SetXY($x, $y);
    $pdf->Write(0, frieda_pdf_form_style_text($phone));

    $pdf->SetFont('Arial', 'B', 14);

    $x = 68.5;
    $y = 114.25;
    $dx = 9.75;
    $dy = 5.35;

    $pdf->SetXY($x + 0*$dx, $y);
    $pdf->Write(0, $dateTimeStart->format('d'));

    $pdf->SetXY($x + 1*$dx, $y);
    $pdf->Write(0, $dateTimeStart->format('m'));

    $pdf->SetXY($x + 2*$dx, $y);
    $pdf->Write(0, $dateTimeStart->format('Y'));

    $pdf->SetXY($x + 0*$dx, $y + $dy);
    $pdf->Write(0, $dateTimeEnd->format('d'));

    $pdf->SetXY($x + 1*$dx, $y + $dy);
    $pdf->Write(0, $dateTimeEnd->format('m'));

    $pdf->SetXY($x + 2*$dx, $y + $dy);
    $pdf->Write(0, $dateTimeEnd->format('Y'));

    if (!empty($bookingcode)) {
        $x = 20;
        $y = 20.5;
        $pdf->SetFont('Arial', 'I', 10);
        $pdf->SetXY($x, $y);
        $pdf->Write(0, frieda_pdf_form_style_text("- Buchungscode: $bookingcode -"));
    }

    $pdf_content = $pdf->Output('S');

    $filename = "Ausleihformular_" . $dateTimeStart->format('Y-m-d') . ".pdf";

    header('Content-type: application/pdf');
    header('Content-Disposition: attachment; filename="' . $filename . '"');

    // Output the PDF content and stop
    echo $pdf_content;
    exit;

}
add_action('template_redirect', 'frieda_pdf_template_redirect');

function frieda_pdf_form_style_text($input) {
    return iconv('UTF-8', 'windows-1252', $input);
}

function determine_and_set_font($strings, &$pdf, $maxWidth = 67) {

    $fontsizes = [14,13.5,13,12.5,12,11.5,11,10.5,10,9.5,9,8.5,8];

    foreach ($fontsizes as $fontsize) {
        $pdf->SetFont('Arial', 'B', $fontsize);

        foreach ($strings as $string) {
            $string_reencoded = frieda_pdf_form_style_text($string);
            $toowide = ($pdf->GetStringWidth($string_reencoded) > $maxWidth) ? true : false;

            if ($toowide) break;
        }
        if (!$toowide) break;
    }

}

