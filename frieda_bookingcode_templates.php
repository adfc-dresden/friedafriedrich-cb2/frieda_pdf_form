<?php
/*
 * Plugin Name: Frieda & Friedrich Bookingcodehinweis mit Besonderheiten für keysafe-Lastenräder
 * Author: Nils Larsen
 */

require_once("FriedaBookingConfirmation.php");

use CommonsBooking\Repository\Booking;

// use CB2 hook filter 'commonsbooking_template_tag' to substitute BOOKINGCODEHINWEIS(n)
// in e-mail-templates where n is the id of the booking: BOOKINGCODEHINWEIS({{booking:ID}})
function frieda_bookingcode_template_callback($string)
{
    $re = '/BOOKINGCODEHINWEIS\((\d+)\)/';
    if (preg_match($re, $string, $matches) == 1) {
        $booking_id = $matches[1];
        if (is_numeric($booking_id) && (int) $booking_id == $booking_id && get_post_type((int) $booking_id) === 'cb_booking') {
            $booking_id = (int) $booking_id;
            $substitution = frieda_bookingcodehinweis($booking_id);

            $string = preg_replace($re, $substitution, $string);
        }
    }

    return $string;
}
add_filter( 'commonsbooking_template_tag', 'frieda_bookingcode_template_callback', 10, 3 );


function frieda_bookingcodehinweis($booking_id) {
    
    if ( FriedaBookingConfirmation_getMatchingBookingId(FRIEDA_KEYSAFE_ITEMS) ) {
        return frieda_keysafe_bookingcodehinweis($booking_id);
    } else {
        return frieda_default_bookingcodehinweis($booking_id);
    }
}

function frieda_keysafe_bookingcodehinweis($booking_id) {
    //$htmloutput = '<br>' . sprintf( commonsbooking_sanitizeHTML( __( 'Your booking code is: %s', 'commonsbooking' ) ), $this->getBookingCode() ) . '<br>';
    $codes_html = frieda_keysafe_generate_codes_html($booking_id);
    return '<br>' .  commonsbooking_sanitizeHTML("Deine Buchungscodes lauten:<br>$codes_html");
}

function frieda_default_bookingcodehinweis($booking_id) {
    $html = "";

    $booking = Booking::getPostById( $booking_id );

    //if($booking) $html .= "OK,";

    $booking_user = $booking->getUserData();

    $template_objects = [
        'booking'  => $booking,
        'item'     => $booking->getItem(),
        'location' => $booking->getLocation(),
        'user'     => $booking_user,
    ];

    $html .= commonsbooking_sanitizeHTML ( commonsbooking_parse_template ( "{{booking:formattedBookingCode}}", $template_objects ) );

    return $html;
}

?>