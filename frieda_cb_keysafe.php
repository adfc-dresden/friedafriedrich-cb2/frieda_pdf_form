<?php
/*
 * Plugin Name: Frieda & Friedrich Commosbooking Keysafe Extensions
 * Author: Nils Larsen
 */


require_once("FriedaBookingConfirmation.php");


/* For keysafe bikes, we don't use CommonsBooking's create-booking-codes option (it must be disabled
 * to avoid re-generation of codes when saving timeframe). It is desired to have show-booking-codes
 * enabled though. Unfortunately, it get reset when saving the timeframe. The "before" hook
 * for the template booking-single is used to hack it back to "on" */

add_action('commonsbooking_before_booking-single', 'frieda_keysafe_before_booking_single');

function frieda_keysafe_before_booking_single() {
    $bookingId = FriedaBookingConfirmation_getMatchingBookingId(FRIEDA_KEYSAFE_ITEMS);
    
    if ($bookingId <= 0) {
        return;
    }

    update_post_meta($bookingId, 'show-booking-codes', "on");

}

add_action('commonsbooking_after_booking-single', 'frieda_keysafe_booking_single');



/*
Replaces the div after "Buchungscode" (in the example it is the div with <strong>2720292</strong>)  with a div with custom text "TRALA"
<div class="cb-list-content cb-datetime cb-col-30-70">
				<div>Buchungscode</div>
				<div><strong>2720292</strong></div>
			</div>
				</div>
*/


function frieda_keysafe_booking_single() {
    
    $bookingId = FriedaBookingConfirmation_getMatchingBookingId(FRIEDA_KEYSAFE_ITEMS);

    if ($bookingId <= 0) {
        return;
    }
    
    $new_code_html = frieda_keysafe_generate_codes_html($bookingId);


    echo <<<HTML
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.cb-list-content div:contains("Buchungscode") + div strong').html('$new_code_html');
        });
    </script>
    HTML;

}


function frieda_keysafe_generate_codes_html($bookingId) {

    $new_code_html = "";

    $locationId = get_post_meta($bookingId, 'location-id', true);
    $itemId = get_post_meta($bookingId, 'item-id', true);
    
    if ($locationId <= 0 || get_post_type($locationId) !== "cb_location") return $new_code_html;
    if ($itemId <= 0 || get_post_type($itemId) !== "cb_item") return $new_code_html;

    $unix_time_start = get_post_meta($bookingId, 'repetition-start', true);
    $unix_time_end = get_post_meta($bookingId, 'repetition-end', true);

    if (!is_numeric($unix_time_start) || !is_numeric($unix_time_end)) return $new_code_html;
    if ($unix_time_start <= 0 || $unix_time_end <= 0 || $unix_time_end < $unix_time_start) return $new_code_html;

    $startDate = FriedaBookingConfirmation_dateTimeFromUnixtime($unix_time_start)->format('Y-m-d');
    $endDate = FriedaBookingConfirmation_dateTimeFromUnixtime($unix_time_end)->format('Y-m-d');

    global $wpdb;
    $table_name = $wpdb->prefix . 'cb_bookingcodes';

    $sql = $wpdb->prepare(
        "SELECT * FROM $table_name
        WHERE item = %d AND location = %d
        AND date BETWEEN %s AND %s
        ORDER BY date ASC
        ",
        $itemId, $locationId,
        $startDate,
        $endDate
    );
    $bookingCodes = $wpdb->get_results($sql);

    $new_code_html = "";

    if (!$bookingCodes || count($bookingCodes) == 0) {
        $new_code_html .= "Fehler: Keine Codes";
    } else {
        foreach ($bookingCodes as $code_row) {
            $formattedDate = date('d.m.', strtotime($code_row->date));
            $new_code_html .= $code_row->code . " am " . $formattedDate . " von 00:01 bis 23:59 Uhr<br>";
        }
    }
    
    return $new_code_html;
}



?>