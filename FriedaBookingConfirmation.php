<?php


// Item-ids of bikes with keysafe
define("FRIEDA_KEYSAFE_ITEMS", [806]); //Kraftwerk Mitte
define("FRIEDA_NOFORM_ITEMS", [810]); //Alnatura

function FriedaBookingConfirmation_dateTimeFromUnixtime($unixtime) {
    $date_time = new DateTime("@$unixtime");
    $date_time->setTimeZone(new DateTimeZone('GMT'));
    return $date_time;
}


function FriedaBookingConfirmation_getMatchingBookingId($keysafeItemIds) {
    global $post;

    if (!isset($post) || !property_exists($post, 'ID')) return 0;

    $bookingId = $post->ID;

    if ($bookingId <= 0) return 0;

    if (get_post_type($bookingId) !== "cb_booking") return 0;

    if (get_post_status($bookingId) !== "confirmed") return 0;

    $itemId = get_post_meta($bookingId, 'item-id', true);

    if (in_array($itemId, $keysafeItemIds)) {
        return $bookingId;
    } else {
        return 0;
    }
}

?>