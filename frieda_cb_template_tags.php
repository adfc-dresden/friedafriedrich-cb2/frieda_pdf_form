<?php
/**
 * Plugin Name: Frieda & Friedrich custom cb2 E-Mail Template Tags: AUSLEIHFORMULARHINWEIS(n)
 * Author: Nils Larsen
 * Author URI: https://www.friedafriedrich.de
 */
 
require_once("FriedaBookingConfirmation.php");

// use CB2 hook filter 'commonsbooking_template_tag' to substitute AUSLEIHFORMULARHINWEIS(n)
// in e-mail-templates where n is the id of the booking: AUSLEIHFORMULARHINWEIS({{booking:ID}})
function frieda_cb_template_tags_callback($string)
{
    $re = '/AUSLEIHFORMULARHINWEIS\((\d+)\)/';
    if (preg_match($re, $string, $matches) == 1) {
        $booking_id = $matches[1];
        if (is_numeric($booking_id) && (int) $booking_id == $booking_id && get_post_type((int) $booking_id) === 'cb_booking') {
            $booking_id = (int) $booking_id;
            $substitution = frieda_cb_template_tags_ausleihformularhinweis($booking_id);

            $string = preg_replace($re, $substitution, $string);
        }
    }

    return $string;
}
add_filter( 'commonsbooking_template_tag', 'frieda_cb_template_tags_callback', 10, 3 );


function frieda_cb_template_tags_ausleihformularhinweis($booking_id) {

    $itemId = get_post_meta($booking_id, 'item-id', true);

    if (in_array($itemId, FRIEDA_KEYSAFE_ITEMS)) {
        return ""; // Kein Ausleihformular für Keysafe-Lastenräder
    }
    
    if (in_array($itemId, FRIEDA_NOFORM_ITEMS)) {
        return ""; // Kein Ausleihformular für Lastenräder für die kein Ausleihformular vorgesehen sind
    }

    $homeurl = home_url();
    $post_name_raw = get_post_field('post_name', $booking_id);

    $post_name = filter_var($post_name_raw, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[a-z0-9]{24}$/')));

    $form_url = sanitize_url("$homeurl/?ausleihformular=$post_name");

    return "<i>Drucke bitte deinen eigenen Abholzettel aus. Dann geht's vor Ort schneller: <a href='$form_url'>Link zum Formular als PDF</a></i>";

}

?>